'use strict';

const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');

class SQLBuildEngine {
    //constructor(database, user, password, schema = "dbo", verbose = false, prependfile = true, appendGo = true, runOnceSkip = false, versioning = false, ssversion = "SSBEVersion", sschanges = "SSBEChanges"){
    constructor(database, verbose = false, prependfile = true, appendGo = true, runOnceSkip = false, versioning = false, ssversion = "SSBEVersion", sschanges = "SSBEChanges"){
        this.Database = database;
        //this.User = user;
        //this.Password= password;
        //this.Schema = schema;
        //SSBEVersion = $"{schema}.{ssversion}";
        //SSBEChanges = $"{schema}.{sschanges}";
        this.SSBEVersion = ssversion;
        this.SSBEChanges = sschanges;
        this.Verbose = verbose;
        this.PrependFile = prependfile;
        this.AppendGo = appendGo;
        this.ExecuteOnceSkip = runOnceSkip;
        this.Versioning = versioning
    
        this.Files = [];
        this.Filenames = [];
        this.Vars = [];
        this.FileVars = [];
    
        this.Commands = [];
    }
    
    async Run()
    {
        await this.Connect();

        while (this.Commands.length > 0)
        {
            var cmd = this.Commands.shift();
            await cmd.Run(this);
        }
    }

    Prepend(cmd)
    {
        this.Commands.push(cmd);
    }
    PrependInclude(commands)
    {
        var reverseCommands = commands.reverse();
        for(var i = 0; i < reverseCommands.length; i++)
        {
            this.Prepend(reverseCommands[i]);
        }
    }

    Add(cmd)
    {
        this.Commands.push(cmd);
    }

    Include(includedCommands)
    {
        includedCommands.forEach(cmd => {
            this.Add(cmd);
        });
    }

    Success(msg)
    {
        // show success Trace
        console.log("SUCCESS", msg);
    }

    GetConnection(){
        if(!this.Connection)
        {
            this.Connect();
        }

        return this.Connection;
    }

    async Connect()
    {
        if (!this.Connection)
        {
            // feedback
            console.log("CONNECT", this.Database);

            // if (Trusted)
            // {
            //     ConnectionString = ConstructConnectionString(Server, Database, Trusted);
            // }
            // else
            // {
            //     ConnectionString = ConstructConnectionString(Server, Database, User, Password);
            // }

            this.Connection = new sqlite3.Database(this.Database + ".db");

            //this.SetupSchema();

            if (this.Versioning)
            {
                await this.SetupVersioning();
            }
        }
    }

    Disconnect()
    {
        console.log("DISCONNECT", Database);

        // close out any open files
        // foreach (StreamWriter sw in Files)
        // {
        //     sw.Close();
        // }

        // mark last version
        if (this.Versioning)
        {
           this.VersionSucceeded();
        }

        Connection.Dispose();
    }

    GetFiles(file)
    {
        // needed vars
        var myfiles = [];

        // check if this is all files in a dir
        if (file.indexOf("*") > 0)
        {
            // get the position of the final dir slash
            var i = ((file.indexOf("/") > -1) ? file.lastIndexOf("/") : file.lastIndexOf("\\")) + 1;
            var files = file.substring(0, i);

            var readout = fs.readdirSync(files);
            readout.forEach(v => { myfiles.push(files + v)});
        }
        else
        {
            // an array of one
            myfiles.push(file);
        }

        // return the results
        return myfiles;
    }

    Substitute(ev)
    {
        let txt = this.SubstituteEngineVars(ev);
        this.Vars.forEach(s =>
        {
            txt.Replace(s.VariableName, s.Value);
        });
        
        return txt;
    }

    SubstituteEngineVars(output)
    {
        //output = output.Replace("$Server$", Server);
        output = output.replace("$Database$", this.Database);
        //output = output.Replace("$User$", User);
        //output = output.Replace("$Password$", Password);
        //output = output.Replace("$Trusted$", Trusted.ToString());
        output = output.replace("$Verbose$", this.Verbose.toString());
        output = output.replace("$PrependFile$", this.PrependFile.toString());
        output = output.replace("$AppendGo$", this.AppendGo.toString());
        output = output.replace("$ExecuteOnceSkip$", this.ExecuteOnceSkip.toString());
        output = output.replace("$Versioning$", this.Versioning.toString());
        //output = output.Replace("$Schema$", Schema.ToString());
        output = output.replace("\r\n", " ");

        return output;
    }

    async ExecuteSQL(sql)
    {
        var statments = sql.split("GO");
        statments.forEach(async statement => {
            await this.ExecuteSQLStatement(statement);
        });
    }
    async ExecuteSQLStatement(sql)
    {
        var connection = this.GetConnection();
        //var fn = connection.run;
        if (!sql)
            return;

        // FileVars.forEach(file => {
        //     if (sql.Contains("@" + file.Name))
        //     {
        //         command.Parameters.Add("@" + file.Name, SqlDbType.VarBinary).Value = file.FileBytes;
        //     }
        // });

        // if (values) {
        //     fn = fn.bind(connection, sql, values);
        // } else {
        //     fn = fn.bind(connection, sql);
        // }
        return new Promise((resolve, reject) => {
            try {
                connection.exec(sql, (err) => {
                    if (err) {
                        reject(
                            console.error(
                                //`Error running query: ${sql} - ${JSON.stringify(values)}: ${err}`,
                                `Error running query: ${sql} : ${err}`,
                            ),
                        );
                    } else {
                        resolve();
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    async GetDataSet(sql)
    {
        var connection = this.GetConnection();
        //var fn = connection.get;
        if (!sql)
            return;

        // if (values) {
        //     fn = fn.bind(connection, sql, values);
        // } else {
        //     fn = fn.bind(connection, sql);
        // }

        //fn = fn.bind(connection, sql);

        return new Promise((resolve, reject) => {
            try {
                connection.get(sql, (t, rows, err) => {
                    if (err) {
                        //var errMsg = `Error running query: ${sql} - ${JSON.stringify(values)}: ${err}`;
                        var errMsg = `Error running query: ${sql} : ${err}`;
                        console.error(errMsg, err);
                        reject(errMsg);
                    } else {
                        resolve(rows);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    // async GetCatalogName()
    // {
    //     // send back the name of the db
    //     var ds = await this.GetDataSet("SELECT DB_NAME() AS DBName");
    //     return ds.Tables[0].Rows[0]["DBName"].ToString();
    // }

    // SetCatalog()
    // {
    //     // update the catalog
    //     Database = this.GetCatalogName();
    // }

    async SetupSchema()
    {
        var ds = await this.GetDataSet(`SELECT SCHEMA_NAME AS TName FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '${this.Schema}'`);
        var needsSchema = true;
        console.log(ds);

        if (ds && ds.Tables.Count == 1)
        {
            ds.Tables[0].Rows.forEach (row =>
            {
                // check if we need the DBBVersion
                if (row["TName"].ToString().ToUpper() == Schema.ToUpper())
                {
                    needsSchema = false;
                }
            });
        }

        if (needsSchema)
        {
            //ExecuteSQL($"CREATE SCHEMA {Schema}");
        }
    }

    async SetupVersioning()
    {
        // check to see this is not a special db
        //var db = await this.GetCatalogName();
        // var curDB = db.toUpperCase();
        // if (curDB == "MASTER" || curDB == "MSDB" || curDB == "TEMPDB" || curDB == "MODEL")
        // {
        //     console.log("WARNING", "Cannot mark a system DB for versioning (currently [" + curDB + "])");
        // }
        // else
        // {

            // get the list of expected tables (dbo.DBBVersion & dbo.DBBChanges)
            var ds = await this.GetDataSet(`SELECT name AS TName FROM sqlite_master WHERE type='table' AND name = '${this.SSBEVersion}'`);
            var cs = await this.GetDataSet(`SELECT name AS TName FROM sqlite_master WHERE type='table' AND name = '${this.SSBEChanges}'`);

            console.log(ds, cs);

            // if not there, create DBBVersion
            if (!ds || !ds.TName || ds == undefined)
            {
                // create table
                await this.ExecuteSQL(`CREATE TABLE ${this.SSBEVersion} (InstalledOn datetime NOT NULL CONSTRAINT PK_${this.SSBEVersion.replace('.', '_')} PRIMARY KEY, LastUpdateOn datetime NOT NULL, CurrentState nvarchar(30) NOT NULL)`);

                // set initial version info
                await this.ExecuteSQL(`INSERT INTO ${this.SSBEVersion} (InstalledOn, LastUpdateOn, CurrentState) VALUES (datetime('now', 'utc'), datetime('now', 'utc'), 'INITIALIZING')`);
            }

            // if not there, create DBBChanges
            if (!cs || !cs.TName || cs == undefined)
            {
                // create table
                await this.ExecuteSQL(`CREATE TABLE ${this.SSBEChanges} (Script nvarchar(450) NOT NULL CONSTRAINT PK_${ this.SSBEChanges.replace('.', '_') } PRIMARY KEY, ChangeState nvarchar(30) NOT NULL, StartedOn datetime NOT NULL, CompletedOn datetime NULL)`);
            }

        //}

    }

    async VersionStart()
    {
        await this.ExecuteSQL(`UPDATE ${this.SSBEVersion} SET CurrentState = 'BUILDING'`);
    }

    async VersionSucceeded()
    {
        await this.ExecuteSQL(`UPDATE ${this.SSBEVersion} SET LastUpdateOn = datetime('now', 'utc'), CurrentState = 'SUCCESSFUL'`);
    }

    async VersionFailed()
    {
        await this.ExecuteSQL(`UPDATE ${this.SSBEVersion} SET LastUpdateOn = datetime('now', 'utc'), CurrentState = 'FAILED'`);
    }

    async ChangeCheck(script)
    {
        var exists = false;

        var ds = await this.GetDataSet(`SELECT Count(*) AS Count FROM ${this.SSBEChanges} WHERE Script = '${script}'`);

        // check for the existance of
        if (ds && !ds.Error && ds.Count && ds.Count > 0)
        {
            exists = true;
        }

        // return result
        return !exists;

    }

    async ChangeStart(script)
    {
        await this.ExecuteSQL(`INSERT INTO ${this.SSBEChanges} (Script, ChangeState, StartedOn) VALUES ('${script}', 'BUILDING', datetime('now', 'utc'))`);
    }

    async ChangeSucceeded(script)
    {
        await this.ExecuteSQL(`UPDATE ${this.SSBEChanges} SET ChangeState = 'SUCCEEDED', CompletedOn = datetime('now', 'utc') WHERE Script = '${script}'`);
    }

    async ChangeFailed(script)
    {
        await this.ExecuteSQL(`DELETE FROM ${this.SSBEChanges} WHERE Script = '${script}'`);
    }
}

class ISqlBuildCommand {
    async Run(be) {}
}

class Append extends ISqlBuildCommand
{
    constructor(file)
    {
        this.File = file;
    }

    async Run(be)
    {
        // Trace
        console.log("APPEND", this.File);

        // open up a new stream
        //StreamWriter sw = new StreamWriter(this.File, true);
        //be.Files.push(sw);
        be.Filenames.push(this.File);
    }
}

class Close extends ISqlBuildCommand
{
    constructor(file)
    {
        this.File = file;
    }

    async Run(be)
    {
        for (var i = 0; i < be.Filenames.Count; i++)
        {
            if (be.Filenames[i].ToString() == File)
            {
                // Trace
                console.log("CLOSE", File);

                var sw = be.Files[i];
                fs.close(sw, (err) => { console.log(`Error closing ${err}`); } );
            }

            be.Files = [];
            be.Filenames = [];
        }
    }
}

// class Cmd extends ISqlBuildCommand
// {
//     cmd = "";
//     constructor(cmd)
//     {
//         this.cmd = cmd;
//     }

//     Run(be)
//     {
//         // Trace
//         console.log("CMD", cmd);

//         var file = cmd.substring(0, cmd.indexOf(' '));
//         var arg = cmd.substring(cmd.indexOf(' ') + 1);
//         var proc = new ProcessStartInfo();
//         proc.CreateNoWindow = true;
//         proc.Arguments = arg;
//         proc.FileName = file;
//         proc.RedirectStandardOutput = true;
//         proc.RedirectStandardError = true;
//         proc.RedirectStandardOutput = true;
//         proc.UseShellExecute = false;
//         Process p = Process.Start(proc);
//         p.WaitForExit();
//     }
// }

class Create extends ISqlBuildCommand
{
    constructor(file)
    {
        super();
        this.file = file;
    }
    async Run(be)
    {
        // Trace
        console.log("CREATE", file);

        fs.open(file, 'r', (err, fd) => {
            be.Files.push(fd);
            be.Filenames.push(file);
        });
    }
}

class Execute extends ISqlBuildCommand
{
    constructor(cmd)
    {
        super();
        this.cmd = cmd;
    }

    async Run(be)
    {
        try
        {
            await this.RunInternal(be);
        }
        catch (ex)
        {
            console.error(ex);
            console.log("ERROR", "Processing File: '" + this.cmd + "'");
        }
    }

    async RunInternal(be){
        if (this.cmd.Contains("/*") || this.cmd.Contains("\\*"))
        {
            await this.RunMultipleFiles(be, this.cmd);
            return;
        }

        // Trace
        console.log("EXECUTE", this.cmd);

        fs.access(this.cmd, fs.constants.F_OK | fs.constants.R_OK, async (err) => {
            if(err){
                await this.RunStatement(be, this.cmd);
            }
            else{
                await this.RunSingleFile(be, this.cmd);
            }
        });
    }

    async RunSingleFile(be, file)
    {
        await this.RunStatement(be, fs.readFileSync(file, 'utf8'));
    }

    async RunMultipleFiles(be, files)
    {
        // get the list of files
        var myfiles = be.GetFiles(files);

        // loop through the files
        myfiles.forEach (async file => {
            console.log("EXECUTE", file);

            await this.RunSingleFile(be, file);
        });
    }

    async RunStatement(be, cmd)
    {
        // run var fix
        var content = be.Substitute(cmd);

        // send to DB
        await be.ExecuteSQL(content);

        // update the catalog
        //be.SetCatalog();
    }
}

class ExecuteOnce extends Execute
{
    constructor(cmd)
    {
        super(cmd);
    }

    async Run(be)
    {
        if (!be.Versioning)
        {
            console.error("Versioning must be true if using ExecuteOnce");
            return;
        }

        await super.Run(be);
    }

    async RunInternal(be)
    {
        if (this.cmd.indexOf("/*") > -1 || this.cmd.indexOf("\\*") > -1)
        {
            await this.RunMultipleFiles(be, this.cmd);
            return;
        }

        try
        {
            // check if this file has been run
            if (await be.ChangeCheck(this.cmd))
            {
                try
                {
                    // Trace
                    console.log("EXECUTE ONCE", this.cmd);

                    // mark start
                    await be.ChangeStart(this.cmd);
                    // send to DB
                    if (!be.ExecuteOnceSkip)
                    {
                        fs.access(this.cmd, fs.constants.F_OK | fs.constants.R_OK, async (err) => {
                            if(err){
                                await this.RunStatement(be, this.cmd);
                            }
                            else{
                                await this.RunSingleFile(be, this.cmd);
                            }
                        });
                    }
                    else
                    {
                        console.log("WARNING", "File was logged but did not run.");
                    }

                    // mark success
                    await be.ChangeSucceeded(this.cmd);

                    // update the catalog
                    //be.SetCatalog();
                }
                catch (Exception)
                {
                    // mark fail
                    //await be.ChangeFailed(this.cmd);
                }
            }
        }
        catch (ex)
        {
            console.error(ex);
        }
    }

    async RunMultipleFiles(be, files)
    {
        // get the list of files
        var myfiles = be.GetFiles(files);

        // loop through the files
        myfiles.forEach(await (async file => {
            // Trace
            console.log("EXECUTE ONCE", file);

            try
            {
                // check if this file has been run
                if (await be.ChangeCheck(file))
                {
                    try
                    {
                        // mark start
                        await be.ChangeStart(file);
                        // send to DB
                        if (!be.ExecuteOnceSkip)
                        {
                            await this.RunSingleFile(be, file);
                        }
                        else
                        {
                            console.log("WARNING", "File was logged but did not run.");
                        }

                        // mark success
                        await be.ChangeSucceeded(file);

                        // update the catalog
                        //be.SetCatalog();
                    }
                    catch (e)
                    {
                        console.error(e)
                        // mark fail
                        await be.ChangeFailed(file);
                    }
                }
            }
            catch (ex)
            {
                console.error(ex);
            }
        }));
        
    }
}

class Exit extends ISqlBuildCommand
{
    async Run(be)
    {
        // clean up loose ends
        be.Disconnect();

        // die
        //Environment.Exit(0);
    }
}

class Set extends ISqlBuildCommand
{
    /// <summary>
    /// Sets a variable name that will be replaced in files
    /// </summary>
    /// <param name="name"></param>
    /// <param name="value"></param>
    constructor(name, value)
    {
        super();
        this.Name = name.Trim();
        this.Value = value.Trim();
    }

    VariableName(){ return `$${this.Name}$`; }
    async Run(be)
    {
        be.Vars.Add(this);
    }
}

class SetFile extends ISqlBuildCommand
{
    VariableName(){ return `$${this.Name}$`; }

    /// <summary>
    /// Sets a variable name that will be replaced in files
    /// </summary>
    /// <param name="name"></param>
    /// <param name="value"></param>
    constructor(name, filePath)
    {
        super();
        Name = name.Trim();
        var data = [];

        fs.open(filePath, (err, fd) => {
            if(err){
                fs.close(fd);
            }
            else{
                fs.read(fd, (err, bytesRead, buffer) => {
                    if(!err){
                        data = buffer;
                    }
                    fs.close(fd);
                });
            }
        });

        this.FileBytes = data;
    }

    async Run(be)
    {
        be.Vars.push(new Set(this.VariableName, `@${this.Name}`));
        be.FileVars.push(this);
    }
}

// class Test extends ISqlBuildCommand
// {
//     string file;
//     public Test(string file) { super(); this.file = file; }

//     public void Run(SQLBuildEngine be)
//     {
//         console.log("TEST", file);

//         int ExitCode = 0;
//         TextWriter se = Console.Error;
//         StreamReader sr = new StreamReader(file);
//         string ln = sr.ReadLine();
//         while (ln != null)
//         {
//             if (ln.StartsWith("Msg "))
//             {
//                 ExitCode = 1;
//                 se.WriteLine(ln);

//             }
//             ln = sr.ReadLine();
//         }
//         se.Close();
//         Environment.Exit(ExitCode);
//     }
// }

// class Write extends ISqlBuildCommand
// {
//     string cmd;
//     public Write(string cmd) { super(); this.cmd = cmd; }

//     public void Run(SQLBuildEngine be)
//     {
//         // var to hold the currently processing file
//         string currentFile = "";

//         try
//         {

//             // get the list of files
//             string[] myfiles = be.GetFiles(cmd);

//             // loop through the files
//             foreach (string file in myfiles)
//             {
//                 // Trace
//                 console.log("WRITE", file);

//                 // set current file
//                 currentFile = file;

//                 // read in the file
//                 StreamReader sr = new StreamReader(file);

//                 // run var fix
//                 string content = be.Substitute(sr.ReadToEnd());

//                 // send to all open outputs
//                 foreach (StreamWriter sw in be.Files)
//                 {

//                     // optionaly out the file name
//                     if (be.PrependFile)
//                     {
//                         sw.WriteLine("PRINT '" + file.Replace("'", "''") + "'");
//                         sw.WriteLine("GO");
//                         sw.WriteLine("");
//                     }

//                     // output the input
//                     sw.WriteLine(content);
//                     sw.WriteLine("");

//                     // check for ending with go
//                     if (be.AppendGo)
//                     {
//                         sw.WriteLine("GO");
//                         sw.WriteLine("");
//                     }
//                 }
//             }

//         }
//         catch (Exception ex)
//         {
//             console.log("ERROR", ex.Message);
//             console.log("ERROR", "Processing File: '" + currentFile + "'");
//             throw;
//         }
//     }
// }

module.exports = { 
    SQLBuildEngine, Append, Close, Create, Execute, ExecuteOnce, Exit, Set, SetFile
}